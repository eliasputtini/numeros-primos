#include "TestaPrimo.h"
#include <time.h>
#include <math.h>

int ehDivisaoExata(int dividendo, int divisor) {
    return (dividendo % divisor) == 0;
}

int ehPrimo(int num) {
  int i, quantDivisores = 0;

  for(i = 1; i <= num; i++) {
      if (ehDivisaoExata(num, i)) {
          quantDivisores++;
      }
  }
  if (quantDivisores == 2) {
      return 1;
  } else {
      return 0;
  }
}

void ehPrimo2(int n){
  int flag =0;
  int i=0;
  for (i = 2; i <= n / 2; ++i) {

        // condition for non-prime
        if (n % i == 0) {
            flag = 1;
            break;
        }
    }

    if (n == 1) {
        printf("1 is neither prime nor composite.\n");
    }
    else {
        if (flag == 0)
            printf("%d is a prime number.\n", n);
        else
            printf("%d is not a prime number.\n", n);
    }
}

int ehPrimo3(int num) {
  if (num <= 3) {
    printf("%d is not a prime number.\n", num > 1); 
    return num>1;
  }
  
  
  if ((num % 2 == 0) || (num % 3 == 0)){
    printf("%d is not a prime number.\n", num);
    return num;
  } 
  
  int count = 5;
  
  while (pow(count, 2) <= num) {
    if (num % count == 0 || num % (count + 2) == 0) {
      printf("%d is not a prime number.\n", num);
      return 0;
    }
    
    count += 6;
  }
  
  printf("%d is a prime number.\n", num);
  return 1;
}


void desvio(float media, float vetor[30]){
    float soma, result ;
    soma=0;
    result=0;

    for(int i = 0; i < 30; i++){
        soma = soma + pow(vetor[i] - media,2);
    }
    result = sqrt((soma)/30);
    printf("Resultado d = %.2f\n", result);
}

void media(float array[30]){
  int i=0;
  float soma = 0;
  for(i=0;i<30; i++){
    soma = soma + array[i];
    
     
  }
  soma = soma /30;
  printf("\nMédia: %f\n",soma);

  desvio(soma,array); 
}




int main() {

  float array[30];
  float array2[30];
  float array3[30];
  //  Verifica o tempo de execução.
  clock_t tempo;
  tempo = clock();
  int i =0;


  for(i=0;i<30; i++){

    printf("\n\né primo? %i\n",ehPrimo(3531271));
    printf("Tempo:%f\n\n",(clock() - tempo)*1000 / (double)CLOCKS_PER_SEC);

    array[i]= (clock() - tempo)*1000 / (double)CLOCKS_PER_SEC;

    ehPrimo2(3531271);
    printf("Tempo2:%f\n\n",(clock() - tempo)*1000 / (double)CLOCKS_PER_SEC);

    array2[i]= (clock() - tempo)*1000 / (double)CLOCKS_PER_SEC;

    ehPrimo3(3531271);
    printf("Tempo3:%f\n",(clock() - tempo)*1000 / (double)CLOCKS_PER_SEC);

    array3[i]= (clock() - tempo)*1000 / (double)CLOCKS_PER_SEC;

  }
  media(array);
  media(array2);
  media(array3);


  return 0;
}